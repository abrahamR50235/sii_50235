// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include<iostream>



//////////////////////////////////////////////////////////////////////
// Implementación Thread
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            read(keyPipe, cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

	close(dataPipe);
	close(keyPipe);
}

void CMundo::InitGL()
{
	//Inicializamos el fifo
	//rutaFifo="/tmp/fifoPuntos";
	//strcpy(rutaFifo, "/tmp/fifoPuntos");
	//puntos="";
	
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	//perror("primer ontimer");
	/*char * w;	
	strcpy(w,"w");
	char * s;	
	strcpy(w,"s");*/


//perror("sec ontimer");
	


//const unsigned char  w = reinterpret_cast<const unsigned char >( "w" );
	//const unsigned char  s = reinterpret_cast<const unsigned char >( "s" );
	//strncpy(w,reinterpret_cast<const unsigned char *>("W"),1);
	//strncpy(s,reinterpret_cast<const unsigned char *>("S"),1);
	
	//Actualizo mi estructura
	//perror("ontimer");
	
	//misDatos.accion = datos->accion;
	//perror("asigné");
	//misDatos.esfera = esfera;
	//misDatos.raqueta1 = jugador1;
	//perror("Pude leer");

	//////////////////////////////////////////////////////////////////////////
	/////////////////// MOVIMIENTO RAQUETA 1 /////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	//std::cout<<"antes de leer";
	if(datos->accion==1){
		CMundo::OnKeyboardDown('w',1,1);
	}else if(datos->accion==-1){
		CMundo::OnKeyboardDown('s',1,1);
	}
	//std::cout<<"después de leer";
	
	//////////////////////////////////////////////////////////////////////////
	/////////////////// MOVIMIENTO RAQUETA 2 /////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	if(datos->accion2==1){
		CMundo::OnKeyboardDown('o',1,1);
	}else if(datos->accion2==-1){
		CMundo::OnKeyboardDown('l',1,1);
	}



	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve();
	datos->raqueta1 = jugador1;
	datos->raqueta2 = jugador2;
	datos->esfera=esfera;
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		if(puntos2==3){
			//kill(logger, 0);
			exit (1);
		}
		esfera.radio=0.5f;
		esfera.mov=0.025f;
		//Escribo en la tubería
		//puntos ="El jugador 1 ha anotado 1 punto";
		strcpy(mensaje, "El jugador 2 ha anotado 1 punto");
		write(fdf,mensaje, 80);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		if(puntos1==3){
			//kill(logger, 0);
			exit (1);
		}
		esfera.radio=0.5f;
		esfera.mov=0.025f;
		strcpy(mensaje, "El jugador 1 ha anotado 1 punto");
		write(fdf,mensaje, 80);
	}
	
		//Datos al cliente.
	sprintf(datosPipe,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	write(dataPipe,datosPipe,200);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	//fifo
	char * dir;
	fdf = open("/tmp/fifoPuntos",O_WRONLY| O_TRUNC); 
	//fdlog = open ("/tmp/logPID", O_RDWR);
	//read(fdlog, pidAux, sizeof(logger));
	//close(fdlog);
	//logger = atoi(pidAux);
	//Se crea el fichero de datos memcompartida
	//perror("entré");
	fdmap= open("/tmp/MiEstruct",  O_CREAT | O_TRUNC |O_RDWR,0666);
	if (fdmap == -1) {
	perror("Error opening file for reading");
   	exit(1); }
	if(write(fdmap,new (DatosMemCompartida), sizeof(DatosMemCompartida))==-1){perror("No se puede");exit(1);}

	dir =(char *)( mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE|PROT_READ,MAP_SHARED, fdmap,(off_t) 0));
	if (dir == MAP_FAILED) {
    perror("Could not mmap");
	exit(1);
  }
	//perror("yuju");
	close(fdmap);
	//perror("cerré");
	/*datos->accion = 1;
	datos->esfera = esfera;
	datos->raqueta1 = jugador1;*/
       // perror("escribe");

	datos = (DatosMemCompartida *) dir;


	//Pipe datos
	dataPipe = open("/tmp/fifopipe",O_WRONLY,0777);
	
	//Pipe teclas
	keyPipe = open("/tmp/fifoteclas",O_RDONLY,0777);

	//Creación del thread.
	pthread_create(&thid1, NULL, hilo_comandos, this);


	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
}
